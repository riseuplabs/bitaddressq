COINS = {
  "btc" => {
    "file" => "btc-addresses.txt",
    "prefix" => "bitcoin:",
    "name" => "Bitcoin (BTC)"
  },
  "btc-segwit" => {
    "file" => "btc-segwit-addresses.txt",
    "prefix" => "bitcoin:",
    "name" => "Bitcoin with SegWit (BTC)"
  },
  "fair" => {
    "file" => "fair-addresses.txt",
    "prefix" => "faircoin:",
    "name" => "FairCoin (FAIR)"
  },
  "bch" => {
    "file" => "bch-addresses.txt",
    "prefix" => "bitcoincash:",
    "name" => "BCash (BCH)"
  }
}

configure :production do
  ADDRESS_DIRECTORY = "/srv/addresses/"
  NOTIFY_EMAIL = 'root@riseup.net'
  RECIPIENT = 'Riseup'
  # a warning email is sent if addresses drop below this number:
  LOW_ADDRESS_COUNT = 50
end

configure :test, :development do
  ADDRESS_DIRECTORY = File.dirname(__FILE__) + '/addresses/'
  NOTIFY_EMAIL = '%s@localhost' % `whoami`.strip
  RECIPIENT = 'TEST'
  LOW_ADDRESS_COUNT = 0
end
