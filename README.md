# Bitcoin Address Queue

This is a very minimal web application to display a unique bitcoin reception
address each time the page is refreshed. It does this by drawing from a pool
of pre-generated addresses (so you don't need to store your private key on the
server).

## Configuration

Edit the file config.rb to specify where the file lives with the bitcoin
addresses, one address per line.

## Running

This web application uses Sinatra and Rack. To run in a development environment:

    RACK_ENV=development ruby app.rb

Then browse to `http://0.0.0.0:4567/index.html`

To run in production deployment using apache and mod_passenger:

    <VirtualHost *:80>
      ServerName www.yourapplication.com
      DocumentRoot /path/to/bitaddressq/public
      <Directory /path/to/bitaddressq/public>
        Require all granted
        Allow from all
        Options -MultiViews
      </Directory>
    </VirtualHost>

## Todo

Not much really, this is dead simple. But maybe:

* Add localization
* Add tests, using random keys (base 58 encode random binary data, see
  https://en.bitcoin.it/wiki/Base58Check_encoding)
